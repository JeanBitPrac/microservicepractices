package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.beans.PostResponse;
import com.example.demo.cart.Cart;
import com.example.demo.checkOut.CheckOutDTO;
import com.example.demo.checkOut.CheckOutService;
import com.example.demo.order.BuyerOrder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Api(value="onlinestore")
@RestController
public class MarketBuyController {
	@Autowired
	CheckOutService checkOutService;
	@Autowired 
	MarketBuyService mService; 
	
	@GetMapping("/")
	  public String home() {
	    return "This is a trivial service that demonstrates how a Eureka Client can register with a Eureka Server";
	  }
	
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@ApiOperation(value = "list order history of a member", response = List.class)
    @ApiResponse(code = 401, message = "You are not authorized to view the resource")
	@RequestMapping(value="/auth/v1/orderHistory/", method=RequestMethod.GET)
	public List<BuyerOrder> getAllOrderHistoryv1(){
		return mService.getAllOrderHistory(SecurityContextHolder.getContext().getAuthentication().getName());
	}
	
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@ApiOperation(value = "list cart of a member", response = List.class)
    @ApiResponse(code = 401, message = "You are not authorized to view the resource")
	@RequestMapping(value="/auth/v1/cart/", method=RequestMethod.GET)
	public List<Cart> getAllCartv1(){
		return mService.getAllCart(SecurityContextHolder.getContext().getAuthentication().getName());
	}
	
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @ApiOperation(value = "add item to cart", response = ResponseEntity.class)
    @ApiResponse(code = 401, message = "You are not authorized to manipulate the resource")
	@RequestMapping(value="/auth/v1/cart", method=RequestMethod.POST)
	public ResponseEntity<Object> addCartv1(@RequestParam("item")String itemId){
		if(mService.addCart(itemId, SecurityContextHolder.getContext().getAuthentication().getName())){
			return ResponseEntity.status(HttpStatus.CREATED).build();	
		}
		else return ResponseEntity.status(HttpStatus.CONFLICT).build();
	}
	
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@ApiOperation(value = "member check out from member cart", response = PostResponse.class)
	@RequestMapping(method=RequestMethod.POST, value="/auth/v1/checkoutCart")
	public PostResponse checkOutCartv1(@RequestBody CheckOutDTO checkOut){
		return checkOutService.checkOut(checkOut, SecurityContextHolder.getContext().getAuthentication().getName(), "");
	}
	
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@ApiOperation(value = "member check out specified item ", response = PostResponse.class)
	@RequestMapping(method=RequestMethod.POST, value="/auth/v1/checkoutItem")
	public PostResponse checkOutItemv1(@RequestBody CheckOutDTO checkOut , @RequestParam(value="item", required=false) String itemId){
		return checkOutService.checkOut(checkOut, SecurityContextHolder.getContext().getAuthentication().getName(), itemId);
	}
	
}
