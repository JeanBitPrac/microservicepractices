package com.example.demo.checkOut;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.beans.Card;
import com.example.demo.beans.CardValidateInfoDto;
import com.example.demo.beans.Item;
import com.example.demo.beans.PostResponse;
import com.example.demo.beans.Shipment;
import com.example.demo.cart.Cart;
import com.example.demo.cart.CartRepository;
import com.example.demo.order.BuyerOrder;
import com.example.demo.order.BuyerOrderRepository;

@Service
public class CheckOutService {
	@Autowired
	CartRepository cartRepository;
	@Autowired 
	BuyerOrderRepository buyerOrderRepository;
	@Autowired
	RestTemplate restTemplate;
	public PostResponse checkOut(CheckOutDTO checkOutDto, String userId, String itemId){
		//check if the service requester is the user(has user authentication or something)
		//check if cart exist
		PostResponse res = new PostResponse();
		res.setSuccess(false);
		res.setCheckoutInfo(checkOutDto);
		List<Cart> carts = new ArrayList<Cart>();
		//see if check out from cart or check out directly
		if(!itemId.equals("")){//check out directly
			Cart cart = new Cart();
			cart.setItemNumber(itemId);
			cart.setUserId(userId);
			carts.add(cart);
			res.setComment("direct check out to"+cart.getItemNumber()+", "+cart.getUserId());
		}
		else{
			try{
				carts = cartRepository.findAllByUserId(userId);
			}catch (Exception e){
				res.setComment("nothing in cart");
				return res;
			}
		}
		//send Request to creditcareService for credit card validation
		try{
			if(creditCardValication(checkOutDto.getCardNumber(), checkOutDto.getSecurityNumber(), checkOutDto.getCardHolder())!=true){
				res.setComment("credit card validation fails");
				return res;
			}
		}
		catch(CreditCardValidationException e){
			res.setComment("exception caught from validation service");
			return res;
		}
		carts.stream().forEach(p ->{
			double price = 0;
			try {	
				price = itemPriceLookUp(p.getItemNumber());
				}
			catch(ItemException e){
				res.setComment("exception caught from item price look up for item "+p.getItemNumber());
				return;
			}
			catch(ItemNotFoundException e){
				res.setComment("exception caught from item not found "+p.getItemNumber());
				return;
			}
			BuyerOrder order = new BuyerOrder();
			order.setItemNumber(p.getItemNumber());
			order.setPrice(price);
			order.setDate(new Date(System.currentTimeMillis()));
			//hard code: carrier = Fedex, fee = 10, estimate shipment day = today + 3 day
			order.setShipment(new Shipment("Fedex",checkOutDto.getShipAddress(), 10, new Date(System.currentTimeMillis()+259200000)));
			order.setUserId(p.getUserId());
			try{	
				buyerOrderRepository.save(order);
			}
			catch(Exception e1) {
				res.setComment("error in saving order");
			}
			try {
				cartRepository.delete(p);
			}
			catch(Exception e2){
				buyerOrderRepository.delete(order);
				res.setComment("error in deleting cart");
			}

		});
		res.setSuccess(true);
		return res;
		
	}
	//send Request to creditcareService for credit card validation
	public boolean creditCardValication(String cardNumber, String securityNumber, String cardHolder)throws CreditCardValidationException{
		RestTemplate rest = new RestTemplate();
	    String url = "https://jean-creditcard-service.herokuapp.com/creditcard/validation-service";
        //setting up the request headers
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        Card card = new Card();
        card.setCardHolder(cardHolder);
        card.setCardNumber(cardNumber);
        card.setSecurityNumber(securityNumber);
        
        //request entity is created with request body and headers
        HttpEntity<Card> requestEntity = new HttpEntity<>(card, requestHeaders);
        ResponseEntity<CardValidateInfoDto> responseEntity;
       try{ responseEntity = rest.exchange(
                url,
                HttpMethod.POST,
                requestEntity,
                CardValidateInfoDto.class
        );
       }
       catch(Exception e1){
    	   try{
    		   url = "http://localhost:8080/creditcard/validation-service";
    		   responseEntity = rest.exchange(
   	                url,
   	                HttpMethod.POST,
   	                requestEntity,
   	                CardValidateInfoDto.class
   	        );
    	   }
    	   catch (Exception e2){
    		   throw  new CreditCardValidationException();
    	   }
       }
        if(responseEntity.getStatusCode() == HttpStatus.OK){
            CardValidateInfoDto validation = responseEntity.getBody();
            System.out.println("user response retrieved " + validation.getDetail());
            return validation.isValid();
         }
        return true;
	}
	public double itemPriceLookUp(String itemNumber)throws ItemException, ItemNotFoundException{
		ResponseEntity<Item> responseObj;
		try{
			responseObj = restTemplate.getForEntity("http://jean-itemlookup-service/items/"+itemNumber, Item.class);
		}
		catch(Exception e2){
				throw new ItemException();
		}
		if(responseObj.getStatusCode().equals(HttpStatus.OK)){
			return responseObj.getBody().getPrice();
		}
		else{
			throw new ItemNotFoundException();
		}
	}
	
}
