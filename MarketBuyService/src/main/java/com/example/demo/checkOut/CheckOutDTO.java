package com.example.demo.checkOut;

import org.springframework.transaction.annotation.Transactional;

import com.example.demo.cart.Cart;

@Transactional
public class CheckOutDTO {
	private String cardNumber;
	private String cardHolder;
	private String securityNumber;
	private String billingAddress;
	private String shipAddress;
	public CheckOutDTO(){};
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public String getShipAddress() {
		return shipAddress;
	}
	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}
	public String getSecurityNumber() {
		return securityNumber;
	}
	public void setSecurityNumber(String securityNumber) {
		this.securityNumber = securityNumber;
	}
	
	
}
