package com.example.demo.beans;

import javax.persistence.Embedded;

import com.example.demo.checkOut.CheckOutDTO;

public class PostResponse {
	private String comment;
	private boolean success;
	@Embedded
	private CheckOutDTO checkoutInfo;
	public PostResponse() {
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public CheckOutDTO getCheckoutInfo() {
		return checkoutInfo;
	}
	public void setCheckoutInfo(CheckOutDTO checkoutInfo) {
		this.checkoutInfo = checkoutInfo;
	}
	
	
}
