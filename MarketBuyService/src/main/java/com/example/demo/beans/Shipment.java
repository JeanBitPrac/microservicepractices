package com.example.demo.beans;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Embeddable
public class Shipment {
	private String carrier;
	private String address;
	private double fee;
	private Date estimateDate;
	public Shipment(){}
	public Shipment(String carrier, String address, double fee, Date estimateDate) {
		this.carrier = carrier;
		this.address = address;
		this.fee = fee;
		this.estimateDate = estimateDate;
	}
	
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public Date getEstimateDate() {
		return estimateDate;
	}
	public void setEstimateDate(Date estimateDate) {
		this.estimateDate = estimateDate;
	}
}
