package com.example.demo.order;

import java.sql.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.example.demo.beans.Shipment;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "All details about the Order. ")
@Entity
public class BuyerOrder {
	@GeneratedValue
	@Id
	private long id;
	private String userId;
	private String itemNumber;
	private double price;
	private Date date;
	@Embedded
	private Shipment shipment;
	public BuyerOrder(){}
	public long getId(){
		return id;
	}
	public void setId(long id){
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Shipment getShipment() {
		return shipment;
	}
	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
