package com.example.demo.order;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface BuyerOrderRepository extends PagingAndSortingRepository<BuyerOrder, Long>{    
	public Iterable<BuyerOrder> findAllByUserId(String id);
}
