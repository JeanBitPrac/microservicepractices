package com.example.demo.authorization;

import org.springframework.security.core.AuthenticationException;

public class UsernameIsExitedException extends AuthenticationException {
	public UsernameIsExitedException(String msg) {
		super(msg);
		System.out.print(msg);
	}
    public UsernameIsExitedException(String msg, Throwable t) {
        super(msg, t);
		System.out.print(msg);
    }


}
