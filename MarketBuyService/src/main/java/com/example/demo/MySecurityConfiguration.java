package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.example.demo.authorization.JWTAuthenticationFilter;
@EnableWebSecurity
@Configuration
public class MySecurityConfiguration extends WebSecurityConfigurerAdapter  {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.httpBasic().disable();
       // http.authorizeRequests()
       //   .antMatchers("/**").hasIpAddress("127.0.0.1:8765");
        //disable csrf for apigateway and swagger ui access REALLY DANGEREOUS!!!!
     	http
         .cors().and().csrf().disable()
         .authorizeRequests()
         .antMatchers("/v2/api-docs", "/swagger-resources/configuration/ui", "/swagger-resources", "/swagger-resources/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()
         .and()
         .authorizeRequests().anyRequest().authenticated().and().addFilter(new JWTAuthenticationFilter(authenticationManager()));
    }
}