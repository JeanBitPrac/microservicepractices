package com.example.demo;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.example.demo.cart.Cart;
import com.example.demo.cart.CartRepository;
@EnableDiscoveryClient
@SpringBootApplication
public class MarketBuyServiceApplication {
	@Autowired
	CartRepository cartRepository;
	public static void main(String[] args) {
		SpringApplication.run(MarketBuyServiceApplication.class, args);
	}
    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
/*	@Bean
	InitializingBean iniBean(){
		return ()->{
			cartRepository.save(new Cart("1", "3rerw3"));
			cartRepository.save(new Cart("1", "23renr"));
			cartRepository.save(new Cart("2", "23kfsg"));

		};
	} 
*/	
}

