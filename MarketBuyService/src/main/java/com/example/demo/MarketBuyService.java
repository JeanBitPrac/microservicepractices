package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.cart.Cart;
import com.example.demo.cart.CartRepository;
import com.example.demo.order.BuyerOrder;
import com.example.demo.order.BuyerOrderRepository;

@Service
public class MarketBuyService {

	@Autowired 
	BuyerOrderRepository bRepository;
	@Autowired
	CartRepository cRepository;
	List<BuyerOrder> blist = null;
	List<Cart> clist = null;
	public List<BuyerOrder> getAllOrderHistory(String id) {
		blist = new ArrayList<BuyerOrder>();
		bRepository.findAllByUserId(id).forEach(blist::add);
		return blist;
	}
	public List<Cart> getAllCart(String id){
		return cRepository.findAllByUserId(id);
	}
	public boolean addCart(String itemId, String userId) {
		try{
			Cart cart = new Cart();
			cart.setItemNumber(itemId);
			cart.setUserId(userId);
			cRepository.save(cart);
			return true;
		}
		catch(Exception e){
			return false;
		}
	}


}
