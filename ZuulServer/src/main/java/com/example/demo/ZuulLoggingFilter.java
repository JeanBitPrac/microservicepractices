package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class ZuulLoggingFilter extends ZuulFilter{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
	public Object run() throws ZuulException {
		// your filter execution logic
		HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
		log.info("request -> {} request uri -> {}", request, request.getRequestURI());
		return null;
	}

	@Override
	public boolean shouldFilter() {
		// when should your filter executed like checking request or other things
		return true;
	}

	@Override
	public int filterOrder() {
		// your filter priority
		return 1;
	}

	@Override
	public String filterType() {
		// pre, post, error or routing, the time filter execute regarding request
 		return "pre";
	}

}
