## For RestApi Calls
1. check out all the Api Documents in Zuul Swagger document(see following instructions)
2. to make Api calls through Zuul Server, use URIs such as https://jean-zuul-server.herokuapp.com/<service-name/<additional path of service>
3. direct calls from clients to microservices will be disabled in the future. Please be aware of that

##Notice
1. JWT is implemented for user login and marketBuy Service
2. Zuul Deployment is completed. API calls should get routes through zuul server

##What you need to install
1. Spring Tool Suites/ Eclipse with STS plugin

##How to run the service(locally)
1. clone this repository, once clone completed, import it into eclipse or STS.
2. run all the applications "run as -> SpringBoot Application".

##Go to the following address to check out API documentations
### Eureka Server
	https://jean-eureka-server.herokuapp.com
### Zuul Server
    https://jean-zuul-server.herokuapp.com/swagger-ui.html#
### MarketBuyService
    https://jean-zuul-server.herokuapp.com/jean-marketbuy-service/swagger-ui.html#
### ItemLookupService
    Item Controller: Item related operation(GetAllItem, GetItem, ...)
    Review Controller: Review related operation(Add, GetAllReview)
    https://jean-zuul-server.herokuapp.com/jean-itemlookup-service/swagger-ui.html
### UserProfileService(User Login, User Register, User Update, User ForgetPassword)
    https://jean-zuul-server.herokuapp.com/jean-userprofile-service/swagger-ui.html#
##Things to do for this project
### 1. Implement Review Service
	review service is now under ItemLookupService, to be implemented: 1. check if user has order history with item when adding review. 2. several review queries(findByRating, findByTime, sorting,...etc)
### 2. Seller Service
	service for creating seller and item selling, will have to change Item structure
### 3. User Password Reset Page
    once use service resetPassword?email= an email contains links which contains userId and token will be sent to user inbox. ex: http://jean-userprofile-service.herokuapp.com:443/changePassword?id=16&token=9c15b0a6-bbcd-4fae-903d-2fbdf1407011.
    Implement this page which direct user to a valid user password reset page.
