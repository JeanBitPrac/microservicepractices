package com.example.demo;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.user.UserService;
@EnableDiscoveryClient
@SpringBootApplication
public class UserProfileServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserProfileServiceApplication.class, args);
	}
	@Autowired
	UserService userService;
//	@Bean
//	InitializingBean iniBean(){
//		return ()->{
//			userService.userRegister(new UserDTOModel("jency267@gmail.com", "jency267", "jency", "jency267"));
//			userService.userRegister(new UserDTOModel("jency158@hotmail.com", "jency158", "jency", "jency158"));
//		};
//	} 

}

