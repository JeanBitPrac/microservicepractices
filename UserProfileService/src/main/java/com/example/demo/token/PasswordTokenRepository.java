package com.example.demo.token;

import org.springframework.data.repository.CrudRepository;

public interface PasswordTokenRepository extends CrudRepository<PasswordResetToken, Long>{

	public PasswordResetToken findByToken(String token);

}
