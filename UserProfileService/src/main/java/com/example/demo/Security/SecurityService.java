package com.example.demo.Security;

import java.util.Arrays;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.example.demo.token.PasswordResetToken;
import com.example.demo.token.PasswordTokenRepository;
import com.example.demo.user.UserBuyer;

@Service
public class SecurityService {
	@Autowired
	PasswordTokenRepository passwordTokenRepository;
	
	public String validatePasswordResetToken(long id, String token) {
	    PasswordResetToken passToken = passwordTokenRepository.findByToken(token);
	    if ((passToken == null) || (passToken.getUser()
	        .getId() != id)) {
	        return "invalidToken";
	    }
	 
	    Calendar cal = Calendar.getInstance();
	    if ((passToken.getExpiryDate()
	        .getTime() - cal.getTime()
	        .getTime()) <= 0) {
	        return "expired";
	    }
	    return null;
	}
}
