package com.example.demo.Security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserDetails extends User {

	private final String userName;
	public MyUserDetails(String username,String userName, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.userName = userName;
		// TODO Auto-generated constructor stub
	}

	public MyUserDetails(String username, String userName, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userName = userName;
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}
	


}
