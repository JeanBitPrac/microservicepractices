package com.example.demo.Security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UsernameIsExitedException extends AuthenticationException{

	public UsernameIsExitedException(String msg) {
		super(msg);
		System.out.print(msg);
	}
    public UsernameIsExitedException(String msg, Throwable t) {
        super(msg, t);
		System.out.print(msg);
    }


}
