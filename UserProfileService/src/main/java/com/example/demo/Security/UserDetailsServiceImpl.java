package com.example.demo.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.user.UserBuyer;
import com.example.demo.user.UserRepository;
import static java.util.Collections.emptyList;
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
	@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserBuyer user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameIsExitedException("user not exists");
        }
        return new MyUserDetails(Long.toString(user.getId()), user.getUsername(), user.getPassword(), emptyList());
    }

}
