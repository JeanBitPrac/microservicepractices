package com.example.demo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface UserRepository extends CrudRepository<UserBuyer, Long>{
	public UserBuyer findByEmail(String email);
	public UserBuyer findByUsername(String username);
}
