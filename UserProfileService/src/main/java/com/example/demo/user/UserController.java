package com.example.demo.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Security.MyUserDetails;
import com.example.demo.Security.MyUserPrincipal;
import com.example.demo.Security.SecurityService;
import com.example.demo.model.ResponseModel;
import com.example.demo.token.PasswordResetToken;
import com.example.demo.token.PasswordTokenRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;;
@RestController
@Api("user management controller")
@CrossOrigin
public class UserController {
	List<UserBuyer> list;
	@Autowired
	private UserService userService;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private PasswordTokenRepository pr;
	@GetMapping("/")
	  public String home() {
	    return "This is a trivial service that demonstrates how a Eureka Client can register with a Eureka Server";
	  }
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@RequestMapping(value="/hello", method=RequestMethod.GET)
	public String hello(){
        MyUserPrincipal principal = (MyUserPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return "hello "+principal.getUserId()+" "+principal.getUserName();
	}
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public List<UserBuyer> getAllUsers(){
		list = new ArrayList<UserBuyer>();
		userService.getAllUser().forEach(list::add);
		return list;
	}

	@ApiOperation("Login.")
	@PostMapping("/login")
	public void fakeLogin(@ApiParam("User") @RequestParam String email, @ApiParam("Password") @RequestParam String password) {
	    throw new IllegalStateException("This method shouldn't be called. It's implemented by Spring Security filters.");
	}
	@GetMapping("/login")
	public void getLogin() {
	}
	@ApiOperation("Logout.")
	@PostMapping("/logout")
	public void fakeLogout() {
	    throw new IllegalStateException("This method shouldn't be called. It's implemented by Spring Security filters.");
	}
	@ApiOperation("Register")
	@PostMapping("/register")
	public ResponseEntity register(@RequestBody UserBuyer user){
		return userService.userRegister(user);
	}
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@ApiOperation("change email, password or username")
	@PostMapping("/update")
	public ResponseEntity update(@RequestBody UserBuyer user) {
		return userService.updateUser(user);
	}
	
	@ApiOperation("send user forget password urls to user email")
	@RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
	public ResponseModel resetPassword(HttpServletRequest request, 
	@RequestParam("email") String userEmail) {
		return userService.resetPassword(request, userEmail);
	}
	
	@ApiOperation("reset password with valid token and id")
	@RequestMapping(value = "/user/changePassword", method = RequestMethod.POST)
	public ResponseEntity changePassword( 
	  @RequestParam("id") long id, @RequestParam("token") String token, @RequestParam("newPW") String newPW) {
	    String result = securityService.validatePasswordResetToken(id, token);
	    if(result != null )//fail for token validation
	    {
	    	return new ResponseEntity(HttpStatus.UNAUTHORIZED);
	    }
	    userService.resetPassword(id, newPW);
	    return new ResponseEntity(HttpStatus.OK);
	}
//	@GetMapping("/tokens")
//	public List<PasswordResetToken> allToken(){
//		List<PasswordResetToken> re = new ArrayList<PasswordResetToken>();
//		pr.findAll().forEach(re::add);
//		return re;
//	}
}
