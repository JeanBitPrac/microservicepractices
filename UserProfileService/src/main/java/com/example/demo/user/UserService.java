package com.example.demo.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.model.ResponseModel;
import com.example.demo.token.PasswordResetToken;
import com.example.demo.token.PasswordTokenRepository;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired 
	PasswordEncoder passwordEncoder;
	@Autowired
	PasswordTokenRepository passwordResetTokenRepository;
	@Autowired
    private JavaMailSender mailSender;
    @Autowired
    private MessageSource messages;
    @Autowired
    private Environment env;
	private List<UserBuyer>users;
	public ResponseEntity userRegister(UserBuyer user){

		if(userRepository.findByEmail(user.getEmail()) != null){//if email already used
			System.out.println("email already exists ");
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		return new ResponseEntity(HttpStatus.CREATED);
	}
	public List<UserBuyer> getAllUser() {
		users = new ArrayList<UserBuyer>();
		userRepository.findAll().forEach(users::add);
		return users;
	}
	public ResponseEntity updateUser(UserBuyer user) {
		user.setId(Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getName()));
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		return new ResponseEntity(HttpStatus.OK);
	}
	
//---------------sending passwordResetToken to email address----------
	public ResponseModel resetPassword(HttpServletRequest request, String userEmail) {
		UserBuyer user = userRepository.findByEmail(userEmail);
		if (user == null) {
		    throw new UserNotFoundException();
		}
		String token = UUID.randomUUID().toString();
		createPasswordResetTokenForUser(user, token);
		mailSender.send(constructResetTokenEmail(getAppUrl(request), 
		  request.getLocale(), token, user));
		return new ResponseModel(200, "success");
	}
	public void createPasswordResetTokenForUser(UserBuyer user, String token){
	    PasswordResetToken myToken = new PasswordResetToken(token, user);
	    passwordResetTokenRepository.save(myToken);
	}
	private SimpleMailMessage constructResetTokenEmail(
			  String contextPath, Locale locale, String token, UserBuyer user){
			    String url = contextPath + "/changePassword?id=" + 
			      user.getId() + "&token=" + token;
			    //String message = messages.getMessage("message.resetPassword", null, locale);
			    return constructEmail("Reset Password", " \r\n" + url, user);
			}
	private SimpleMailMessage constructEmail(String subject, String body, 
	  UserBuyer user) {
	    SimpleMailMessage email = new SimpleMailMessage();
	    email.setSubject(subject);
	    email.setText(body);
	    email.setTo(user.getEmail());
	    email.setFrom(env.getProperty("support.email"));
	    return email;
	}
    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
	public void resetPassword(long id, String newPW) {
		Optional<UserBuyer> user = userRepository.findById(id);
		if(!user.isPresent()) {
			throw new UserNotFoundException();
		}
		user.get().setPassword(passwordEncoder.encode(newPW));
		userRepository.save(user.get());
	}
}
