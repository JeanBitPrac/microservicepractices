package com.example.demo;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.example.demo.item.Item;
import com.example.demo.item.ItemRepository;
@EnableEurekaClient
@EnableDiscoveryClient
@EnableAutoConfiguration
@SpringBootApplication
public class ItemLookUpServiceApplication {
	
	@Autowired
	private ItemRepository itemRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(ItemLookUpServiceApplication.class, args);
	}
	/*@Bean 
	InitializingBean  initialBean(){
		return ()->{
			itemRepository.save(new Item("3rerw3", "iphoneSE", "electronics", 290.00));
			itemRepository.save(new Item("1rw324", "iphone6", "electronics", 350.00));
			itemRepository.save(new Item("124nrw", "iphone6Plus", "electronics", 399.99));
			itemRepository.save(new Item("23kfsg", "Adorable Fox Plush", "toy", 12.99));
			itemRepository.save(new Item("wese3r", "Ocean World Whale Plush", "toy", 8.99));
			itemRepository.save(new Item("32ef3t", "Lining Jean jeans", "clothes", 15.99));
			itemRepository.save(new Item("23renr", "Lining Jean Top Boots", "clothes", 13.50));

		};
	}*/
}

