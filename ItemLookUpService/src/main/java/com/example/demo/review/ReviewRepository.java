package com.example.demo.review;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ReviewRepository extends PagingAndSortingRepository<Review, Long> {
	public List<Review>  findByItemId(String itemId, Pageable pageable);
	
	@Query("SELECT AVG(u.rating) FROM Review u WHERE u.item.id = ?1")
	public Double averageRating(String itemId);
}
