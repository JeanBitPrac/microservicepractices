package com.example.demo.review;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.filter.MyUserPrincipal;
import com.example.demo.item.Item;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
public class ReviewController {
	@Autowired
	ReviewService reviewService;
	@ApiOperation(value = "get reviews of the item")
	@RequestMapping(value="/{itemId}/reviews", method=RequestMethod.GET)
	public List<Review> getReviews(@PathVariable("itemId") String itemId, @RequestParam("size") int size, @RequestParam("page") int page) {
		return reviewService.getReviews(itemId, size, page);
	}
	@ApiOperation(value = "add review to the item")
	@ApiImplicitParam(name = "token", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@RequestMapping(value="/{itemId}/reviews", method=RequestMethod.POST)
	public ResponseEntity addReview(@PathVariable("itemId") String itemId, @Valid @RequestBody  Review review) {
		Item item = new Item();
		item.setId(itemId);
		review.setItem(item);
		MyUserPrincipal user = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		review.setUserId(Long.parseLong(user.getUserId()));
		review.setUsername(user.getUserName());
		reviewService.addReview(review);
		return new ResponseEntity(HttpStatus.CREATED);
	}
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity handleException(MethodArgumentNotValidException ex) {
		return new ResponseEntity(HttpStatus.BAD_REQUEST);
	}
}
