package com.example.demo.review;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.item.Item;
import com.example.demo.item.ItemRepository;

@Service
public class ReviewService {
	@Autowired
	ReviewRepository reviewRepository;
	@Autowired 
	ItemRepository itemRepository;
	public List<Review> getReviews(String itemId, int size, int page) {
		Pageable pageable = PageRequest.of(page, size);
		return reviewRepository.findByItemId(itemId, pageable);
	}
	public double getRating(String itemId) {
		Double rating =0.0;
		rating = reviewRepository.averageRating(itemId);
		if(rating == null) {
			rating = 0.0;
		}
		return rating;
	}
	public void addReview(Review review) {
		Optional<Item> item = itemRepository.findById(review.getItem().getId());
		if(item.isPresent()) {
			int reviewCount = item.get().getReviewCount();
			float rating = item.get().getRating();
			item.get().setRating(((rating*reviewCount)+review.getRating())/(reviewCount+1));
			item.get().setReviewCount(reviewCount+1);
			itemRepository.save(item.get());
		}
		reviewRepository.save(review);
	}

}
