package com.example.demo.item;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api("Item services")
@RestController
public class ItemController {
	@Autowired
	ItemService itemService;	
	@RequestMapping(value="/items", method=RequestMethod.GET)
	public List<Item> getAllItems(){
		return itemService.getAllItems();
	}

	@GetMapping("/")
	  public String home() {
	    return "This is a trivial service that demonstrates how a Eureka Client can register with a Eureka Server";
	  }
	
	@RequestMapping(value="items/{id}", method=RequestMethod.GET)
	public ResponseEntity<Item> getItem(@PathVariable("id") String id){
		try {
			return new ResponseEntity<Item>(itemService.getItem(id), HttpStatus.OK);
		}
		catch(Exception e){
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	@RequestMapping(value="/items", method=RequestMethod.POST)
	public ResponseEntity<Item> addItem(@RequestBody Item item) {
		boolean result = itemService.addItem(item);
		if(result) {
			return new ResponseEntity<Item>(item, HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<Item>(item, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/items/{id}", method=RequestMethod.DELETE)
	@ApiOperation(value = "Item delete, will need authorize in the future", response = ResponseEntity.class)
	public ResponseEntity deleteItem(@PathVariable("id") String id){
		try{
			itemService.deleteItem(id);
		}catch(IllegalArgumentException e) {
			return new ResponseEntity<String>("id is null or item does not exist", HttpStatus.OK);
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@RequestMapping(value="/items/{id}", method=RequestMethod.PUT)
	@ApiOperation(value = "Item update, will need authorize in the future", response = ResponseEntity.class)
	public ResponseEntity updateItem(@PathVariable("id") String id, @RequestBody Item item){
		item.setId(id);
		itemService.updateItem(item);
		return new ResponseEntity(HttpStatus.OK);
	}
}
