package com.example.demo.item;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
@ApiModel(description = "All details about the Item. ")
@Entity
public class Item {
	@Id
	@NotNull
	@NotEmpty
	private String id;
	@NotNull
	@NotEmpty
	private String name;
	private String category;
	private double price;
	private float rating;
	private int reviewCount;
	
	public Item(){}
	
	public String getId() {
		return id;
	}
	public void setId(String itemId) {
		this.id = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}
	
}
