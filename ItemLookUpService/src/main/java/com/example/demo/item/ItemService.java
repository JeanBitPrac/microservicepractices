package com.example.demo.item;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ItemService {
	
	@Autowired
	ItemRepository itemRepository;
	List<Item> list;
	public  List<Item> getAllItems() {
		return itemRepository.findAll();
		
	}
	public List<Item> getAllItems(int offset, int pgeNum){
		Pageable page = PageRequest.of(pgeNum, offset);
		return itemRepository.findAll(page).getContent();
	}
	public Item getItem(String id) {
		return itemRepository.findById(id).get();
	}
	public boolean addItem(Item item) {
		try {
			itemRepository.save(item);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	public void deleteItem(String id) {
		itemRepository.deleteById(id);		
	}
	public void updateItem(Item item) {
		itemRepository.save(item);
	}
	
}
