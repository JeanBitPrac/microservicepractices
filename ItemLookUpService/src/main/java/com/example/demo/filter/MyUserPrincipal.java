package com.example.demo.filter;

import java.security.Principal;

public class MyUserPrincipal implements Principal {
	private final String userId;
	private final String userName;
	
	public MyUserPrincipal(String userId, String userName) {
		super();
		this.userId = userId;
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return userId;
	}

}
