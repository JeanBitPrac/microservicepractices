package com.example.demo.filter;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UsernameIsExitedException extends AuthenticationException {

	public UsernameIsExitedException(String string) {
		super(string);
	}

}
