package com.example.demo.card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.card.DTO.CardValidateInfoRequestDTO;
import com.example.demo.card.DTO.CardValidateInfoResponseDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("credit card services")
@RestController
@RequestMapping("/creditcard")
public class CreditCardController {
	@Autowired 
	CreditCardService cardService;
	@ApiOperation(value = "credit card validation", response = CardValidateInfoResponseDto.class)
	@RequestMapping(value="/validation-service", method=RequestMethod.POST,produces="application/json")
	public CardValidateInfoResponseDto cardValidator(@RequestBody CardValidateInfoRequestDTO card){
		return cardService.validate(card);
	}
}
