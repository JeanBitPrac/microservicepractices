package com.example.demo.card.DTO;

public class CardValidateInfoResponseDto {
	private String last4Digits; // should only contain last 4 digits
	private boolean isValid;
	private String detail;
	public CardValidateInfoResponseDto(String last4Digits, boolean isValid, String detail) {
		this.last4Digits = last4Digits.substring(last4Digits.length()-4);
		this.isValid = isValid;
	}
	public String getLast4Digits() {
		return last4Digits;
	}
	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits.substring(last4Digits.length()-4);
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
}
