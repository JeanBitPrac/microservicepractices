package com.example.demo.card;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
@Repository
public interface CreditCardRepository extends CrudRepository<CreditCard, Long>{
	public CreditCard findByCardNumber(String num);
}
