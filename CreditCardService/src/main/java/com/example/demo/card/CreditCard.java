package com.example.demo.card;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class CreditCard {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String cardHolder;
	private String cardNumber;
	private String securityNumber;
	private	Date expDate;
	private double totalCredit;
	private double statementBalance;
	private double availableCredit;
	
	public CreditCard(){}
	public CreditCard(String cardHolder, String cardNumber, String securityNumber) {
		this.cardHolder = cardHolder;
		this.cardNumber = cardNumber;
		this.securityNumber = securityNumber;
	}
	public CreditCard(String cardHolder, String cardNumber, String securityNumber, Date expDate,
			double totalCredit, double statementBalance, double availableCredit) {
		this.cardHolder = cardHolder;
		this.cardNumber = cardNumber;
		this.securityNumber = securityNumber;
		this.expDate = expDate;
		this.totalCredit = totalCredit;
		this.statementBalance = statementBalance;
		this.availableCredit = availableCredit;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSecurityNumber() {
		return securityNumber;
	}
	public void setSecurityNumber(String securityNumber) {
		this.securityNumber = securityNumber;
	}
	public String getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public double getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(double totalCredit) {
		this.totalCredit = totalCredit;
	}
	public double getStatementBalance() {
		return statementBalance;
	}
	public void setStatementBalance(double statementBalance) {
		this.statementBalance = statementBalance;
	}
	public double getAvailableCredit() {
		return availableCredit;
	}
	public void setAvailableCredit(double availableCredit) {
		this.availableCredit = availableCredit;
	} 
	
}
