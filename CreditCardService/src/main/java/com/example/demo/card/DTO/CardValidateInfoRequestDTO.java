package com.example.demo.card.DTO;
public class CardValidateInfoRequestDTO {
	private String cardHolder;
	private String cardNumber;
	private String securityNumber;
	public CardValidateInfoRequestDTO() {
	}
	public String getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getSecurityNumber() {
		return securityNumber;
	}
	public void setSecurityNumber(String securityNumber) {
		this.securityNumber = securityNumber;
	}
}
