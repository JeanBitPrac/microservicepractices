package com.example.demo.card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.card.DTO.CardValidateInfoRequestDTO;
import com.example.demo.card.DTO.CardValidateInfoResponseDto;

@Service
public class CreditCardService {
	@Autowired 
	CreditCardRepository cardRepository;
	
	public CardValidateInfoResponseDto validate(CardValidateInfoRequestDTO vCard) {
		CreditCard card = cardRepository.findByCardNumber(vCard.getCardNumber());
		CardValidateInfoResponseDto cardV = new CardValidateInfoResponseDto(vCard.getCardNumber(), false, "");
		if(card == null){
			cardV.setDetail("card not found");
		}
		else if(!card.getSecurityNumber().equals(vCard.getSecurityNumber())){
			cardV.setDetail("security number not match");
		}
		else if(card.getCardHolder().compareToIgnoreCase(vCard.getCardHolder())!=0){
			cardV.setDetail("card holder name not match");
		}
		
		else{
			cardV.setValid(true);
		}
		return cardV;
	}
	
}
