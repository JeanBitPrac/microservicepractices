package com.example.demo;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.demo.card.CreditCard;
import com.example.demo.card.CreditCardRepository;

@SpringBootApplication
public class CreditCardServiceApplication {

	@Autowired
	CreditCardRepository cardRepository;
	public static void main(String[] args) {
		SpringApplication.run(CreditCardServiceApplication.class, args);
	}
	@Bean
	InitializingBean  initialBean(){
		return ()->{
			cardRepository.save(new CreditCard("Jean Hsu", "12412443", "113"));
			cardRepository.save(new CreditCard("Judy Hsu", "14366433", "133"));
			cardRepository.save(new CreditCard("Chu-Ching Hsu", "51535235", "221"));
			System.out.println("initailzing bean");
		};
	}
}

